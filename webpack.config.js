// https://webpack.js.org/configuration/
// https://webpack.github.io/docs/configuration.html
// https://github.com/gokulkrishh/how-to-setup-webpack-2
// http://erikaybar.name/webpack-source-maps-in-chrome/?utm_source=javascriptweekly&utm_medium=email
// https://github.com/webpack-contrib/json-loader
// https://www.npmjs.com/package/http-server
// https://webpack.js.org/guides/code-splitting-libraries/
// https://blog.madewithenvy.com/getting-started-with-webpack-2-ed2b86c68783#.bnqkoyhva
// https://github.com/jackfranklin/react-css-modules-webpack
// http://javascriptplayground.com/blog/2016/07/css-modules-webpack-react/
// https://github.com/JaKXz/stylelint-webpack-plugin
// https://web-design-weekly.com/2016/06/15/integrate-stylelint-workflow-better-css/
// https://stylelint.io/user-guide/example-config/
// https://github.com/kristerkari/stylelint-scss

// https://github.com/MoOx/eslint-loader
// https://github.com/feross/eslint-config-standard
// https://gist.github.com/cletusw/e01a85e399ab563b1236
// http://standardjs.com/
// https://gist.github.com/jhartikainen/7dd4124631fcff66b43f
// http://eslint.org/docs/user-guide/configuring

// https://github.com/postcss/postcss
// https://github.com/stylelint/stylelint


// npm info css-loader versions
// yarn info css-loader versions

'use strict';

const path = require('path');
const webpack = require('webpack');
const extractTextPlugin = require('extract-text-webpack-plugin');
const htmlWebpackPlugin = require('html-webpack-plugin');
const styleLintPlugin = require('stylelint-webpack-plugin');

var DEVELOPMENT = process.env.NODE_ENV === 'development';
var PRODUCTION = process.env.NODE_ENV === 'production';

var entry = PRODUCTION
    ?   { index:  ['./src/app/index.js'] }
    :   { index:  ['./src/app/index.js', 'webpack/hot/dev-server', 'webpack-dev-server/client?http://localhost:8080'] };


// https://github.com/jaketrent/html-webpack-template
var plugins = PRODUCTION
    ?   [
            
            // new webpack.optimize.UglifyJsPlugin({
            //     comments: true,
            //     mangle: false,
            //     compress: {
            //         warnings: true
            //     }
            // }),
            new webpack.optimize.UglifyJsPlugin(),
            new extractTextPlugin('style-[contenthash:10].css'),       // This configures the plugin to output to styles-.css.
            new htmlWebpackPlugin({
                template: 'index-template.html'
            })

            ,new styleLintPlugin({
                configFile: '.stylelintrc.json',
                context: path.resolve(__dirname, './src'),
                emitErrors: true,
                failOnError: false,
                //files: '**/*.css',
                // files: '**/*.s?(a|c)ss',     
                files: '**/*.{c,sc,sa,le}ss',           
                quiet: false
            })
        ]
    :   [
            new webpack.HotModuleReplacementPlugin()
            ,new styleLintPlugin({
                configFile: '.stylelintrc.json',
                context: path.resolve(__dirname, './src'),
                emitErrors: true,
                failOnError: false,
                //files: '**/*.css',                
                // files: '**/*.s?(a|c)ss',
                files: '**/*.{c,sc,sa,le}ss',                
                quiet: false
            })
        ];

plugins.push(
    new webpack.DefinePlugin({
        DEVELOPMENT: JSON.stringify(DEVELOPMENT),
        PRODUCTION: JSON.stringify(PRODUCTION)
    })
);

var cssIdentifier = PRODUCTION ? '[hash:base64:10]' : '[path][name]---[local]';
// var cssIdentifier = PRODUCTION ? '[hash:base64:10]' : '[name]__[local]___[hash:base64:10]';
// http://javascriptplayground.com/blog/2016/07/css-modules-webpack-react/

// https://github.com/jsdf/webpack-combine-loaders
var cssLoader = PRODUCTION
    ?   extractTextPlugin.extract([
            {
                // loader: 'css-loader?localIdentName=' + cssIdentifier
                // loader: 'css-loader?modules=true&localIdentName=' + cssIdentifier
                loader: 'css-loader',
                options: {
                    // modules: true,
                    localIdentName: cssIdentifier,
                    importLoaders: 1
                }
            },
            {            
                loader: 'postcss-loader'
            }
        ])
    :   [
            {            
                loader: 'style-loader'
            },
            {            
                // loader: 'css-loader?localIdentName=' + cssIdentifier
                // loader: 'css-loader?modules=true&localIdentName=' + cssIdentifier
                loader: 'css-loader',
                options: {
                    // modules: true,
                    localIdentName: cssIdentifier,
                    importLoaders: 1
                }                             
            },
            {            
                    loader: 'postcss-loader'
            }  
        ];



var sassLoader = PRODUCTION
    ?   extractTextPlugin.extract([
            {
                loader: 'css-loader?localIdentName=' + cssIdentifier
            },            
            {            
                loader: 'sass-loader'
            }
        ])
    :   [
            {            
                loader: 'style-loader'
            },
            {            
                loader: 'css-loader?localIdentName=' + cssIdentifier,
                options: {
                    sourceMap: true
                }               
            },
            {            
                loader: 'sass-loader',
                options: {
                    sourceMap: true
                }
            }
        ];


var devtool = PRODUCTION
    // ?   ''  // No sourcemap for production
    ?   'source-map'

    // :   'source-map';
    :   'eval-source-map' // Default development sourcemap;
     

const config = {

    externals: {
        'jquery': 'jQuery'   // jquery is external and is available at the global variable jQuery
        // import $ from 'jquery';
        // $('#app').css('background-color', 'yellow'); 
    },

  
    resolve: {
        
        // https://github.com/webpack/enhanced-resolve
        // modules: ['node_modules', path.resolve(__dirname, './src')],
        // https://gist.github.com/sokra/27b24881210b56bbaff7#resolving-options
        modules: [path.resolve(__dirname, './src'), 'node_modules'],
        // (was split into `root`, `modulesDirectories` and `fallback` in the old options)
        // In which folders the resolver look for modules
        // relative paths are looked up in every parent folder (like node_modules)
        // absolute paths are looked up directly
        // the order is respected

        // extensions: ['', '.js', '.json'],
        // These extensions are tried when resolving a file

        // mainFiles: ['index'],
        // These files are tried when trying to resolve a directory

        alias: {
            // jquery: path.resolve(__dirname, 'vendor/jquery-2.0.0.js')
        }
        // These aliasing is used when trying to resolve a module
    },

    // devtool: 'source-map',
    devtool: devtool,

	// context: path.resolve(__dirname, './src/'), // 'resolve' think of it as the result of executing cd with each argument
                                                // __dirname refers to the directory where this webpack.config.js lives
    context: path.resolve(__dirname),

    //   entry: path.resolve(SRC, 'app'),                       
    entry: entry,

    //   entry: {
    //      index:  ['./app/index.js', 'webpack/hot/dev-server', 'webpack-dev-server/client?http://localhost:8080']
    //     //  ,vendors: 'moment'
    //   },	
    //   entry: [
    //     //   './src/app/index.js',
    //       './app/index.js',
    //       'webpack/hot/dev-server',
    //       'webpack-dev-server/client?http://localhost:8080'
    //   ],

    plugins: plugins,
//   plugins: [
//       new webpack.HotModuleReplacementPlugin()
//   ],


  output: {
    // path: path.resolve(__dirname, './dist'),
    path: path.resolve(__dirname, './dist/'),
    // publicPath: '/',        
    publicPath: PRODUCTION ? '/' : '/dist/',
    // path: path.resolve(__dirname, './dist/assets/'),
	// publicPath: '/assets',

    // filename: 'index.bundle.js'
    // filename: 'bundle.js'
    // filename: '[name].bundle.js'       // [name] or [id] gets replaced with the object key from entry
    // filename: PRODUCTION ? '[name].[hash].bundle.js' : '[name].bundle.js'      
    filename: PRODUCTION ? '[name].[hash:12].min.js' : '[name].bundle.js'
  },
  
  devServer: {
    open: true, // to open the local server in browser  
    // contentBase: path.resolve(__dirname, './dist/')
    contentBase: path.resolve(__dirname, './src/')
  }

  ,module: {
      rules: [
      {
        enforce: "pre",
        // test: /\.js$/,    
        test: /\.jsx?$/,
        exclude: /node_modules/,
        // loader: "eslint-loader"
        use: [{
          loader: 'eslint-loader'
          //options: { presets: ["es2015", { "modules": false }] }
          
        }],
      },

      {
        // test: /\.js$/,
        test: /\.jsx?$/,
        use: [{
          loader: 'babel-loader'
          //options: { presets: ["es2015", { "modules": false }] }
          
        }],
        // exclude: path.resolve(__dirname, './node_modules/')
        exclude: [
            path.resolve(__dirname, './node_modules/'),
            path.resolve(__dirname, './bower_components/')
        ]
      }
    
      // Loaders for other file types can go here
      ,{
        test: /\.(png|jpg|gif)$/,
        use: [{
            //   loader: 'file-loader'
            loader: 'url-loader?limit=10000&name=images/[hash:12].[ext]'
            // loader: 'url-loader?limit=10000&name=images/[hash:12].[ext]'
            //options: { presets: ["es2015", { "modules": false }] }
          
        }],
        // exclude: path.resolve(__dirname, './node_modules/')
        exclude: [
            path.resolve(__dirname, './node_modules/'),
            path.resolve(__dirname, './bower_components/')
        ]
      }
      ,{
        test: /\.css$/,
        use: cssLoader,
        // use: [{            
        //         loader: 'style-loader'
        //     },
        //     {            
        //         loader: 'css-loader?localIdentName=' + cssIdentifier               
        // }],
        // exclude: path.resolve(__dirname, './node_modules/')
        exclude: [
            path.resolve(__dirname, './node_modules/'),
            path.resolve(__dirname, './bower_components/')
        ]
      }
      ,{
            test: /\.(woff|woff2|eot|ttf|svg)$/,
            use: [
                    {
                        loader: 'url-loader?limit=100000'
                     }
            ],
            // exclude: path.resolve(__dirname, './node_modules/')
            exclude: [
                path.resolve(__dirname, './node_modules/'),
                path.resolve(__dirname, './bower_components/')
            ]    
      }
      ,{
        test: /\.(sass|scss)$/, //Check for sass or scss file names
        // use: [
        //   'style-loader',
        //   'css-loader',
        //   'sass-loader',
        // ],
        use: sassLoader,
        // exclude: path.resolve(__dirname, './node_modules/')
            exclude: [
                path.resolve(__dirname, './node_modules/'),
                path.resolve(__dirname, './bower_components/')
            ]
      }
    ]
  }
};

module.exports = config; 