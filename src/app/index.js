console.log('Hello from index.js');

const style = require('./style/globalStyle.css');

// const messages = require('./messages');

// import $ from 'jquery';
         
// import Button from './button'; 
// import Kitten from './kitten';


var DEVELOPMENT = process.env.NODE_ENV === 'development';
var PRODUCTION = process.env.NODE_ENV === 'production';


// var newMessage = () => (`
//     <p> 
//         ${messages.hi} dfgh ${messages.event}
//         ${Kitten}
//     </p>
// `);
var newMessage = () => (`
    <div class="${style.box}">
        DEV: ${DEVELOPMENT.toString()}<br>
        PROD: ${PRODUCTION.toString()}<br>
    </div>
`);
// var newMessage = () => (Button.button);

var app = document.getElementById('app');
// app.innerHTML = '<p>' + messages.hi + ' ' + messages.event + '</p>';
app.innerHTML = newMessage();


// Button.attachEl();

// $('#app').css('background-color', 'yellow');

if(DEVELOPMENT){
    if(module.hot){
        module.hot.accept();
    }
}

