# webpack2-starter #

A webpack2 starter project with basic configuration.


### **To begin:** ###
First install node.js and then


```
#!javascript

npm upgrade --global yarn
```


or


```
#!javascript

npm install --global yarn
```


```
#!javascript

git clone git@bitbucket.org:zubejo/webpack2-starter.git

yarn install

yarn run dev

yarn run build
```